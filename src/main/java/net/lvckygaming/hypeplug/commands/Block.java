//This class was created by StossenSrc 
//(C)2018 - 2019 LvckyGaming Inc.
//create on 17:54 a clock 
//at the 09.06.2019


package net.lvckygaming.hypeplug.commands;

import net.lvckygaming.hypeplug.main.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Block implements CommandExecutor {
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Player p = (Player) commandSender;
        if(Main.lock== false) {
            if (p.hasPermission("main.show")) {
                p.hasPermission("§6Befehle:");
                p.hasPermission("§a/crea | /spec | /surv | /adv : ändert den Gamemode");
            } else {
                p.sendMessage(Main.p + "§4Dir fehlt volgende Berechtigung:§e main.show");
            }

        }else{
            p.sendMessage(Main.msl);
        }
        return false;
    }
}
