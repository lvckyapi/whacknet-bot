//This class was created by StossenSrc 
//(C)2018 - 2019 LvckyGaming Inc.
//create on 10:57 a clock 
//at the 15.06.2019


package net.lvckygaming.hypeplug.commands;

import net.lvckygaming.hypeplug.main.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Lock_command implements CommandExecutor {
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Player p = (Player) commandSender;
        if(p.hasPermission("*")){
            if(strings.length == 1) {
                if(strings[0].equalsIgnoreCase("lock")){
                    Main.lock = true;
                    p.sendMessage("§4§lAlle HP befehle wurden dektiviert!");
                }else if(strings[0].equals("2016-2019LG-GmbH")){
                    p.sendMessage("§a§lAlle HP Befehle sind nun wieder aktiviert");
                    Main.lock = false;
                }else {
                    p.sendMessage("Mache /lock lock/PIN");
                    p.sendMessage("oder der Pin ist falsch!");
                }
            }else {
                p.sendMessage("Mache /lock lock/PIN");
            }
        }else{
            p.sendMessage("§c§lDu musst Owner sein um diesen Befehl zu können!");
            Bukkit.broadcastMessage("§4ACHTUNG! " + p.getDisplayName() + " §4§lhat den Geheimcommand versucht");
            Bukkit.broadcastMessage("§b§lEMPFOHLEN IST EIN BAN");
        }
        return false;
    }

}
