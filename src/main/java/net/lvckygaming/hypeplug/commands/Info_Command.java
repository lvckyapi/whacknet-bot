//This class was created by StossenSrc 
//(C)2018 - 2019 LvckyGaming Inc.
//create on 10:43 a clock 
//at the 15.06.2019


package net.lvckygaming.hypeplug.commands;

import net.lvckygaming.hypeplug.main.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Info_Command implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player ss = (Player) sender;
        if(Main.lock== false) {
            ss.sendMessage("§cDieser Server benutzt §eHypePlug §cvom §lLvckyGaming Network");
            ss.sendMessage("§cDieser Server benutzt §eHypePlug §cvon der §lLvckyGaming GmbH");
            ss.sendMessage("§aDownload: §bhttp://hypeplug-01.lvckygaming.org");
        }else {
            ss.sendMessage(Main.msl);
        }
        return false;
    }
}
