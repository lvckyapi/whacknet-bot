//This class was created by StossenSrc 
//(C)2018 - 2019 LvckyGaming Inc.
//create on 17:37 a clock 
//at the 09.06.2019


package net.lvckygaming.hypeplug.gamemode;

import net.lvckygaming.hypeplug.main.Main;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class Survival implements CommandExecutor {

    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Player p = (Player)commandSender;
        if(Main.lock== false) {
            if (p.hasPermission("hp.gamemode")) {
                if (strings.length == 0) {
                    p.sendMessage(Main.p + "§6Du bist nun im Survival mode!");
                    p.setGameMode(GameMode.SURVIVAL);
                } else {
                    p.sendMessage(Main.p + "§e Mache /surv");
                    p.setHealth(0);
                }
            } else {
                Bukkit.broadcastMessage(p.getDisplayName() + " Du hast keine Berechtigung für /surv");
                p.sendMessage("§4Die fehlt Berechtigung:§e hp.gamemode");
            }
        }else {
            p.sendMessage(Main.msl);
        }
        return false;
    }
}
