//This class was created by StossenSrc 
//(C)2018 - 2019 LvckyGaming Inc.
//create on 17:30 a clock 
//at the 09.06.2019


package net.lvckygaming.hypeplug.main;

import net.lvckygaming.hypeplug.commands.Block;
import net.lvckygaming.hypeplug.commands.Info_Command;
import net.lvckygaming.hypeplug.commands.Lock_command;
import net.lvckygaming.hypeplug.commands.Test_Command;
import net.lvckygaming.hypeplug.gamemode.*;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements CommandExecutor {

    //String
    public static String p = "§7[§aHypePlug§7] ";
    public static String msl = "§7[§cHypePlug befehle sind deaktiviert§7] ";
    //Boolean
    public static boolean lock;

    @Override
    public void onEnable() {
        Bukkit.getConsoleSender().sendMessage("§7[§aHypePlug§7] §5Aktiviert!");

        //GM
        getCommand("spec").setExecutor(new Spectator());
        getCommand("adv").setExecutor(new Adventure());
        getCommand("crea").setExecutor(new Creativ());
        getCommand("surv").setExecutor(new Survival());
        getCommand("gamemode").setExecutor(new GameMode_Command());
        getCommand("gm").setExecutor(new GameMode_Command());
        //Commands:
        getCommand("plugload").setExecutor(new PluginLoader());
        getCommand("hp").setExecutor(new Info_Command());
        getCommand("lock").setExecutor(new Lock_command());
        getCommand("lmg").setExecutor(new Test_Command());
        //Help ** Plugin Block
        getCommand("pl").setExecutor(new Block());
        getCommand("plugins").setExecutor(new Block());
        getCommand("help").setExecutor(new Block());
        getCommand("plugin").setExecutor(new Block());


    }
}
